var express = require('express');
var bodyParser = require('body-parser')

var url = require('url');

var path = require('path');

var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(bodyParser.json())
app.use('/static', express.static('static'));
app.set('view engine', 'ejs');

app.get('/', function(req, res){
    res.sendFile(path.join(__dirname, 'static/receiver.html'));
});

app.get('/sender', function(req, res){
    if (!req.query.id) {
        return res.status(400).end();
    }

    io.to('/#' + req.query.id).emit('pageRequested');
    res.render('sender', {id: req.query.id});
});

app.post('/events/:clientId', function(req, res) {
    io.to('/#' + req.params.clientId).emit(req.body.eventType, req.body);
    res.status(200).end();
});

app.get('/noscript.gif', function(req, res) {
    try {
        io.to('/#' + req.query.id).emit('jsDisabled');
    } catch (ex) {
        console.error(ex);
    }
    
    res.sendFile(path.join(__dirname, '/static/spacer.gif'));
})

http.listen(3000, function(){
    console.log('listening on *:3000');
});